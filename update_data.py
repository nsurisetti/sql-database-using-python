import db_connection as dbConn;

class Update:
	def func_UpdateData(self):
		# Ge the sql connection
		connection = dbConn.getConnection()

		Empid = input('Enter Employee Id = ')

		try:
			# Fethc the data which needs to be updated
			sql = "Select * From Employee Where Id = ?" 
			cursor = connection.cursor()
			cursor.execute(sql, [Empid])
			item = cursor.fetchone()
			print('Data Fetched for Id = ', Empid)
			print('ID\t\t Name\t\t\t Age')
			print('-------------------------------------------')       
			print(' {}\t\t {} \t\t\t{} '.format(item[0], item[1], item[2]))
			print('-------------------------------------------')
			print('Enter New Data To Update Employee Record ')

			Empname = input('Enter New Name = ')
			department = input('Enter New Age = ')
			query = "Update Employee Set Name = ?, Age =? Where Id =?" 
	
			# Execute the update query
			cursor.execute(query, [Empname, department, Empid])
			connection.commit()
			print('Data Updated Successfully')

		except:
			print('Something wrong, please check')

		finally:
			# Close the connection
			connection.close()