import db_connection as dbConn

class Create:
  def func_CreateData(self):

    # Get the sql connection
    connection = dbConn.getConnection()
            
    Empname = input('Enter Name = ')
    department = input('Enter department = ')

    try:
      query = "Insert Into Employee(Name, department) Values(?,?)" 
      cursor = connection.cursor()

      # Execute the sql query
      cursor.execute(query, [Empname, department])

      # Commit the data
      connection.commit()
      print('Data Saved Successfully')

    except:
      print('Somethng worng, please check')

    finally:
      # Close the connection
      connection.close()